import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NavBarComponent } from './nav/navbar.component';

import {AppComponent} from './app.component';
import { DashboardComponent } from './DecisionStory/decision.dashboard.component';
import { TemplateComponent } from './DecisionStory/decision.template.component';
import { ThumbnailComponent } from './DecisionStory/decision.thumbnail.component';
import { FooterComponent } from './nav/footer.component';
import { BlogComponent } from './HomePage/blog.component';
import { HomebannerComponent } from './HomePage/homebanner.component';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    DashboardComponent,
    TemplateComponent,
    ThumbnailComponent,
    FooterComponent,
    BlogComponent,
    HomebannerComponent

  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
