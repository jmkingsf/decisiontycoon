import { Component } from '@angular/core';

@Component({
  selector: 'events-app',
  template: `
  <navbar></navbar>
  <dashboard-component></dashboard-component>
  <footer></footer>
  
  `,
  styleUrls: []
})
export class AppComponent {
  title = 'app';
}
